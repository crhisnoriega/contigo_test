package com.mcontigo.mycontigotest.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mcontigo.mycontigotest.impl.Song;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONSerDes {

    public List<Song> convertToSongs(String json){
        Gson gson = new Gson();
        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
        JsonElement results = jsonObject.get("results");
        Song[] songs_array = gson.fromJson(results, Song[].class);

        List<Song> songs = new ArrayList<Song>(Arrays.asList(songs_array));

        return songs;
    }
}
