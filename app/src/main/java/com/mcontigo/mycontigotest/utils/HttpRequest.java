package com.mcontigo.mycontigotest.utils;

import android.util.Log;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpRequest {

    public static String MAIN_URL = "https://itunes.apple.com/search?";

    private OkHttpClient client;

    public HttpRequest(){
        this.client = new OkHttpClient();
    }

    public String requestFilter(String filter) throws Throwable {

        HttpUrl.Builder urlBuilder = HttpUrl.parse(MAIN_URL + "entity=musicVideo&term=" + filter).newBuilder();
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        String json = response.body().string();
        return json;
    }
}
