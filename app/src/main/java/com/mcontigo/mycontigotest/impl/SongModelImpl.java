package com.mcontigo.mycontigotest.impl;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mcontigo.mycontigotest.SongPresenter;
import com.mcontigo.mycontigotest.utils.HttpRequest;
import com.mcontigo.mycontigotest.utils.JSONSerDes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class SongModelImpl implements SongPresenter.SongModel {

    private HttpRequest request;
    private JSONSerDes serdes;
    private SongPresenter presenter;

    public SongModelImpl(SongPresenter presenter) {
        this.presenter = presenter;
        this.request = new HttpRequest();
        this.serdes = new JSONSerDes();
    }


    @Override
    public void searchSongs(final String filter) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            private String jsonReponse;
            private Throwable throwable;
            private List<Song> songs;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    this.songs = new ArrayList<Song>();
                    this.jsonReponse = request.requestFilter(filter);
                    this.songs = serdes.convertToSongs(this.jsonReponse);
                } catch (Throwable t) {
                    this.throwable = t;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (this.throwable == null) {
                    presenter.updateList(this.songs);
                    if(this.songs.isEmpty()){
                        presenter.showError("Nothing found!");
                    }
                } else {
                    presenter.showError(this.throwable.getLocalizedMessage());
                }
            }


        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


}
