package com.mcontigo.mycontigotest.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mcontigo.mycontigotest.ShowPreviewActivity;
import com.mcontigo.mycontigotest.SongPresenter;
import com.mcontigo.mycontigotest.ViewState;

import java.util.List;

public class SongPresenterImpl implements SongPresenter {

    private SongListView view;
    private SongModel model;

    private ViewState state;

    public SongPresenterImpl(SongListView view) {
        this.view = view;
        this.state = new ViewState();
        this.model = new SongModelImpl(this);
    }


    @Override
    public void clearViewState() {
        this.state = new ViewState();
    }

    @Override
    public Context getContext() {
        return (Context) this.view;
    }

    @Override
    public void refreshView() {
        this.view.showProgressBar(true);
        this.model.searchSongs(this.state.getFilter());
    }

    @Override
    public void updateList(List<Song> songs) {
        this.view.showProgressBar(false);
        this.view.clearSearchView();
        this.view.updateList(songs);
    }

    @Override
    public void doSearch(String filter) {
        this.state.setFilter(filter);
        this.view.showProgressBar(true);
        this.model.searchSongs(filter);
    }

    @Override
    public void showError(String error) {
        this.view.showProgressBar(false);
        this.view.showError(error);
    }

    @Override
    public void showPreview(String url) {
        Context context = (Context) this.view;
        Intent intent = new Intent(context, ShowPreviewActivity.class);

        Bundle params = new Bundle();
        params.putCharSequence("url", url); //Your id
        intent.putExtras(params);

        context.startActivity(intent);
    }
}
