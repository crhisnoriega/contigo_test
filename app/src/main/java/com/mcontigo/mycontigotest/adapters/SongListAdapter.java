package com.mcontigo.mycontigotest.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mcontigo.mycontigotest.R;
import com.mcontigo.mycontigotest.SongPresenter;
import com.mcontigo.mycontigotest.impl.Song;
import com.mcontigo.mycontigotest.utils.TimeFormater;
import com.squareup.picasso.Picasso;

import org.joda.time.Duration;

import java.util.List;

import butterknife.BindView;

public class SongListAdapter extends RecyclerView.Adapter {

    private List<Song> songs;
    private SongPresenter presenter;

    public SongListAdapter(List<Song> songs, SongPresenter presenter) {
        this.songs = songs;
        this.presenter = presenter;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    public List<Song> getSongs() {
        return songs;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_song_list, viewGroup, false);
        return new SongViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Song item = this.songs.get(position);

        SongViewHolder songHolder = (SongViewHolder) holder;
        Picasso.get().load(item.getArtworkUrl100()).into(((SongViewHolder) holder).icon);
        songHolder.title.setText(item.getTrackName());
        songHolder.artist.setText(item.getArtistName());

        if (!TextUtils.isEmpty(item.getTrackTimeMillis())) {
            songHolder.duration.setText(TimeFormater.formatDuration(Long.parseLong(item.getTrackTimeMillis())));
        }

        songHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(item.getPreviewUrl())) {
                    presenter.showPreview(item.getPreviewUrl());
                } else {
                    presenter.showError("Preview not found");
                }
            }
        });

        if (!TextUtils.isEmpty(item.getTrackPrice()) &&
                Float.parseFloat(item.getTrackPrice()) > 1.5f) {
            songHolder.background_color.setImageDrawable(presenter.getContext().getResources().getDrawable(R.drawable.background_gradient_expensive));
        } else {
            songHolder.background_color.setImageDrawable(presenter.getContext().getResources().getDrawable(R.drawable.background_gradient));
        }
    }

    @Override
    public int getItemCount() {
        return this.songs.size();
    }

    public class SongViewHolder extends RecyclerView.ViewHolder {

        public ImageView background_color;
        public ImageView background_icon;

        ImageView icon;
        TextView title;
        TextView duration;
        TextView artist;


        public SongViewHolder(final View itemView) {
            super(itemView);

            this.background_color = itemView.findViewById(R.id.background);
            // this.background_icon = itemView.findViewById(R.id.background_icon);
            this.icon = itemView.findViewById(R.id.icon);
            this.title = itemView.findViewById(R.id.title);
            this.duration = itemView.findViewById(R.id.duration);
            this.artist = itemView.findViewById(R.id.artist);
        }
    }


}
