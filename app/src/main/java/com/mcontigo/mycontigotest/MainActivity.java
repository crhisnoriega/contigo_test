package com.mcontigo.mycontigotest;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import net.danlew.android.joda.DateUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.mcontigo.mycontigotest.impl.SongPresenterImpl;
import com.mcontigo.mycontigotest.impl.Song;
import com.mcontigo.mycontigotest.adapters.SongListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.support.design.widget.AppBarLayout;
import android.widget.Toast;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class MainActivity extends AppCompatActivity implements SongPresenter.SongListView {

    @BindView(R.id.btnfab)
    FloatingActionButton btnFab;

    @BindView(R.id.songlist)
    RecyclerView songList;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout layout;

    @BindView(R.id.app_bar)
    AppBarLayout barLayout;

    SongPresenter presenter;


    private SearchView searchView;
    private MenuItem searchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // binding
        ButterKnife.bind(this);

        // joda
        JodaTimeAndroid.init(this);

        // toolbar
        setSupportActionBar(this.toolbar);

        // set title
        getSupportActionBar().setTitle("My Playlist");

        // presenter
        if (this.presenter == null) {
            this.presenter = new SongPresenterImpl(this);
        }

        List<Song> list = new ArrayList<Song>();
        if (savedInstanceState != null && savedInstanceState.containsKey("list")) {
            list = (List<Song>) savedInstanceState.getSerializable("list");
        }

        // init list
        this.songList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        this.songList.setAdapter(new SongListAdapter(list, this.presenter));

          }


    @OnClick(R.id.btnfab)
    public void onBtnFabClick(View view) {
        this.presenter.refreshView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflate menu and search event
        getMenuInflater().inflate(R.menu.menu_main, menu);

        this.searchItem = menu.findItem(R.id.action_search);
        this.searchView = (SearchView) this.searchItem.getActionView();

        this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                presenter.doSearch(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                barLayout.setExpanded(false);
                return false;
            }
        });

        ImageView closeButton = (ImageView) this.searchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.clearViewState();
                clearSearchView();
                updateList(new ArrayList<Song>());
            }
        });

        return true;
    }

    // view methods
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        SongListAdapter adapter = (SongListAdapter) this.songList.getAdapter();
        outState.putSerializable("list", (ArrayList) adapter.getSongs());
    }

    @Override
    public void updateList(List<Song> songs) {
        SongListAdapter adapter = (SongListAdapter) this.songList.getAdapter();
        adapter.setSongs(songs);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void showProgressBar(boolean status) {
        this.progressBar.setVisibility(status ? View.VISIBLE : View.GONE);
    }


    @Override
    public void showError(String error) {
        Snackbar.make(this.layout, "Error: " + error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void clearSearchView() {

        if (this.searchView != null) {
            EditText et = (EditText) this.searchView.findViewById(R.id.search_src_text);
            et.setText("");

            this.searchView.setQuery("", false);
            this.searchView.onActionViewCollapsed();
        }

        if (this.searchItem != null) {
            this.searchItem.collapseActionView();
        }

        barLayout.setExpanded(true);
    }
}
