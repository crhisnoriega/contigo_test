package com.mcontigo.mycontigotest;

import com.mcontigo.mycontigotest.impl.Song;

import java.util.List;

public class ViewState {

    private String filter;
    private List<Song> songs;

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
}
