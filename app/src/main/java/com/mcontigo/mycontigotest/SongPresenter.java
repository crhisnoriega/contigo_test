package com.mcontigo.mycontigotest;

import android.content.Context;
import android.os.Bundle;

import com.mcontigo.mycontigotest.impl.Song;

import java.io.Serializable;
import java.util.List;

// presenter
public interface SongPresenter {

    public Context getContext();

    public void updateList(List<Song> songs);

    public void refreshView();

    public void clearViewState();

    public void doSearch(String filter);

    public void showError(String error);

    public void showPreview(String url);

    // view
    interface SongListView {
        public void updateList(List<Song> songs);

        public void showProgressBar(boolean status);

        public void showError(String error);

        public void clearSearchView();
    }

    // model
    interface SongModel extends Serializable {
        public void searchSongs(String filter);
    }
}
