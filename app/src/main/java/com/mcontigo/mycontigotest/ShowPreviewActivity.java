package com.mcontigo.mycontigotest;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mcontigo.mycontigotest.utils.TranslucideBar;

import butterknife.BindView;
import butterknife.ButterKnife;

// notice this is not a view, just embed WebView
public class ShowPreviewActivity extends AppCompatActivity {

    @BindView(R.id.webview)
    WebView webView;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_preview);

        // binding
        ButterKnife.bind(this);

        // translucide
        TranslucideBar.makeTranslucide(this);

        // get url
        Bundle params = getIntent().getExtras();
        String url = (String) params.getCharSequence("url");

        this.progressBar.setVisibility(View.VISIBLE);

        this.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                progressBar.setVisibility(View.GONE);
            }
        });

        this.webView.loadUrl(url);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            this.webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.webView.destroy();
        this.finish();
    }
}
